## SUPER MARKUP WORLD

#### Nombre: Alvin Patrik Bohorquez Uño. C.I. 10590260

- Nivel 1

 `<div style='left:300px;bottom:100px;width:1300px'></div>`

[![Captura-de-pantalla-49.png](https://i.postimg.cc/CxwRkMMs/Captura-de-pantalla-49.png)](https://postimg.cc/D8YfKTXS)

- Nivel 2

`<div style='left:300px;bottom:100px;width:1300px'></div>`

`<div style='left:700px;bottom:150px;width:900px'></div>`

`<div style='left:900px;bottom:200px;width:700px'></div>`

`<div style='left:1100px;bottom:250px;width:500px'></div>`

[![Captura-de-pantalla-50.png](https://i.postimg.cc/fyjwRXdn/Captura-de-pantalla-50.png)](https://postimg.cc/p5rN0mkq)

- Nivel 3

`<div style='left:100px;bottom:100px;width:1500px'></div>`

`<div style='left:150px;bottom:150px;width:900px'></div>`

`<div style='left:200px;bottom:200px;width:700px'></div>`

`<div style='left:250px;bottom:250px;width:500px'></div>`

`<div style='left:300px;bottom:300px;width:300px'></div>`

`<div style='left:350px;bottom:350px;width:200px'></div>`

[![Captura-de-pantalla-51.png](https://i.postimg.cc/DZPDWcxg/Captura-de-pantalla-51.png)](https://postimg.cc/wy3FZD8y)

- Nivel 4

`<div style='left:300px;bottom:100px;width:1300px;background:blue'></div>`

`<div style='left:500px;bottom:150px;width:1100px;background:green'></div>`

`<div style='left:600px;bottom:200px;width:1000px;background:red'></div>`

`<div style='left:700px;bottom:250px;width:900px;background:yellow'></div>`

`<div style='left:800px;bottom:300px;width:800px;background:#40E0D0'></div>`

`<div style='left:900px;bottom:350px;width:700px;background:#CCCCFF'></div>`

`<div style='left:1000px;bottom:400px;width:600px;background:#9FE2BF'></div>`

`<div style='left:1100px;bottom:450px;width:500px;background:#B07467'></div>`

[![Captura-de-pantalla-52.png](https://i.postimg.cc/d1s6fc92/Captura-de-pantalla-52.png)](https://postimg.cc/LqWjq7w5)

- Nivel  5

`<div style='left:300px;bottom:100px;width:500px;background:blue'></div>`

`<div style='left:900px;bottom:150px;width:100px;background:green'></div>`

`<div style='left:1000px;bottom:200px;width:100px;background:red'></div>`

`<div style='left:1100px;bottom:250px;width:100px;background:yellow'></div>`

`<div style='left:900px;bottom:300px;width:100px;background:#40E0D0'></div>`

`<div style='left:800px;bottom:350px;width:100px;background:#CCCCFF'></div>`

`<div style='left:700px;bottom:400px;width:100px;background:#9FE2BF'></div>`

`<div style='left:600px;bottom:450px;width:100px;background:#B07467'></div>`

[![Captura-de-pantalla-53.png](https://i.postimg.cc/htV0Pty8/Captura-de-pantalla-53.png)](https://postimg.cc/sMDWTsR1)
- Nivel 6

`<p style='left:640px;bottom:300px;width:150px'></div>`

[![Captura-de-pantalla-54.png](https://i.postimg.cc/XJ1fDGqW/Captura-de-pantalla-54.png)](https://postimg.cc/vg9x4m52)

- Nivel 7

`<div style='left:300px;bottom:15px;width:800px'></div>`

`<p style='left:1250px;bottom:15px;width:150px'></div>`

`<div style='left:1500px;bottom:130px;width:100px'></div>`

`<p style='left:1650px;bottom:190px;width:100px'></div>`

`<div style='left:1450px;bottom:290px;width:150px'></div>`

`<div style='left:50px;bottom:340px;width:20px'></div>`

`<p style='left:1600px;bottom:440px;width:80px'></div>`

[![Captura-de-pantalla-55.png](https://i.postimg.cc/PJLWGXvw/Captura-de-pantalla-55.png)](https://postimg.cc/YLHGYw3r)